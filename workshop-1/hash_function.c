#include <stdio.h>
#include <stdint.h>

static const uint8_t hashtbl[] = {
	18, 11, 10, 21, 7, 5, 9, 22, 17, 2, 12, 3, 19, 1, 14, 16, 20, 8, 23, 4, 26, 15, 6, 24, 13, 25
};

__asm int generate_hash( const char *str, const uint8_t *hashtbl, int *hashptr )
{
	// Define names for used registers
input_str	RN r0
hashtbl		RN r1
hashptr		RN r2
curr_char	RN r6
hash_val	RN r3

	MOV		hash_val, #0							// Initialize hash_val to zero
hash_loop
	LDRB	curr_char, [input_str]		// Load byte into curr_char from memory pointed to by input_str
	CMP		curr_char, #'0'						// Compare it with 0
	BLS		hash_skip									// If it is lower or same in the ASCII table, then skip this character
	
	CMP		curr_char, #'9'						// Compare it with 9
	SUBLS	hash_val, curr_char				// If byte is lower or same, then subtract its value from the hash_val
	ADDLS	hash_val, #48							// Add 48 to the hash_val because ASCII '0' index is 48 decimal
	BLS		hash_skip									// Then move the the next character
	
	CMP		curr_char, #'A' - 1				// Compare it with the character before 'A'
	BLS		hash_skip									// If it is lower or same in the ASCII table, then skip this character
	
	CMP		curr_char, #'Z'						// Compare it with the 'Z' character
	BHI		hash_skip									// If it is higher in the ASCII table, then skip this character
	
	SUB		r4, curr_char, #65				// Subtract 65 from curr_char because the ASCII 'A' index is 65 decimal and store in r4
	ADD		r4, hashtbl, r4					  // Add the address of the start of the lookup table to r4
	LDRB	r5, [r4]									// Load byte into r5 from memory pointed to by r4 (character hash value)
	ADD		hash_val, r5							// Add the character hash value to the hash_val
	
hash_skip
	ADD		input_str, input_str, #1	// Increment the input_str pointer
	CMP		curr_char, #0							// Check if the byte is 0
	BNE		hash_loop									// If not, repeat loop
	MOVEQ	r0, hash_val							// Else store hash_val to r0
	STR		hash_val, [hashptr]
	BX		lr												// Return from subroutine
}

int main( void )
{
	static const char STRING_TO_HASH[] = "FD7N8JT!EGMIQH2@3ZW0ABCRSLOYV45PK1#X6U!9";
	int hash = 0;
	
	generate_hash(STRING_TO_HASH, hashtbl, &hash);
	printf("%d", hash);
	
	return 0;
}
